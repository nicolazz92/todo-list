package com.example.controller;

import com.example.model.Deal;
import com.example.repository.DealRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.Objects;

import static org.springframework.util.Assert.notNull;

/**
 * Created by nicolas on 30.07.16 at 18:25.
 */
@Controller
@RequestMapping("/deal/")
public class DealController {

    private final DealRepository repo;

    @Autowired
    public DealController(DealRepository repo) {
        notNull(repo);
        this.repo = repo;
    }

    @GetMapping("/")
    public String getAllDeals(Model model) {
        List<Deal> deals = repo.findAll();
        if (!model.containsAttribute("deal_list")) {
            model.addAttribute("deal_list", deals);
        }
        if (!model.containsAttribute("deal")) {
            model.addAttribute("deal", new Deal());
        }

        return "/listing";
    }

    @GetMapping("/status")
    public String getDealsInProcess(Model model, @RequestParam("process") boolean process) {
        List<Deal> deals;
        if (process) {
            deals = repo.findByStatus(0);
        } else {
            deals = repo.findByStatus(1);
        }
        model.addAttribute("deal_list", deals);
        return getAllDeals(model);
    }


    @RequestMapping(value = "/", method = RequestMethod.POST)
    public RedirectView newDeal(Deal deal, @RequestParam("id") Long id) {
        if (id != null) {
            Deal beforeDeal = repo.findOne(id);
            if (!Objects.equals(deal.getName(), "")) {
                beforeDeal.setName(deal.getName());
                beforeDeal.setDescription(deal.getDescription());
                repo.save(beforeDeal);
            }
        } else {
            if (!Objects.equals(deal.getName(), "")) {
                repo.save(deal);
            }
        }
        return new RedirectView("/deal/");
    }

    @RequestMapping("/{id}")
    public String editDeal(Model model, @PathVariable("id") Long id) {
        Deal deal = repo.findOne(id);
        model.addAttribute("deal", deal);
        return getAllDeals(model);
    }

    @RequestMapping("/delete/{id}")
    public RedirectView removeDeal(@PathVariable("id") Long id) {
        repo.delete(id);
        return new RedirectView("/deal/");
    }


    @RequestMapping(value = "/status/{id}", method = RequestMethod.GET)
    public RedirectView makeDealDone(@PathVariable("id") long id) {
        Deal deal = repo.findOne(id);
        deal.setStatus(1);
        repo.save(deal);
        return new RedirectView("/deal/");
    }

    @RequestMapping("/adddeals")
    public RedirectView addTestDeal() {
        repo.save(new Deal("deal1", "description1"));
        repo.save(new Deal("deal2", "description2"));
        repo.save(new Deal("deal3", "description3"));
        repo.save(new Deal("deal4", "description4"));
        repo.save(new Deal("deal5", "description5"));
        return new RedirectView("/deal/");
    }
}
