package com.example.model;

import javax.persistence.*;

/**
 * Created by nicolas on 23.07.16 at 14:55 at 14:55.
 */

@Entity
@Table(name = "dealList")
public class Deal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(length = 500)
    private String description;

    @Column(nullable = false)
    private int status;

    public Deal() {
    }

    public Deal(String name, String description) {
        this.description = description;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", " + name +
                ", description: " + description +
                ", status: " + ((status == 0) ? "in process" : "done");
    }
}
