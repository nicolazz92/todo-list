package com.example.repository;

import com.example.model.Deal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by nicolas on 30.07.16 at 18:23.
 */
public interface DealRepository extends JpaRepository<Deal, Long> {

    List<Deal> findByStatus(int status);
}
