DROP TABLE IF EXISTS dealList;
CREATE TABLE dealList
(
  id     BIGINT PRIMARY KEY NOT NULL IDENTITY,
  description LONGVARCHAR,
  name   VARCHAR(255)       NOT NULL,
  status INT                NOT NULL
);