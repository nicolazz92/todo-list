<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: nicolas
  Date: 26.07.16
  Time: 22:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ToDo List</title>
</head>
<body>

<table>
    <tr>
        <td>
            <a href="/deal/">
                <img src="../static/get_all.png"/>
            </a>
        </td>
        <td>
            <a href="/deal/status?process=true">
                <img src="../../static/get_in_process.png"/>
            </a>
        </td>
        <td>
            <a href="/deal/status?process=false">
                <img src="../../static/get_done.png"/>
            </a>
        </td>
    </tr>
</table>

<table>

    <tr>
        <td>
            <i><b>Name</b></i>
        </td>
        <td>
            <i><b>Description</b></i>
        </td>
    </tr>

    <c:forEach items="${deal_list}" var="deal">
        <tr>
            <td><c:out value="${deal.name}"/></td>
            <td><c:out value="${deal.description}"/></td>
            <td><c:out value="${deal.status==0?'in process':'done'}"/></td>
            <td>
                <a href="/deal/status/${deal.id}">
                    <img src="../static/done.png"/>
                </a>
            </td>
            <td>
                <a href="/deal/${deal.id}">
                    <img src="../static/edit.png"/>
                </a>
            </td>
            <td>
                <a href="/deal/delete/${deal.id}">
                    <img src="../static/delete.png"/>
                </a>
            </td>
        </tr>
    </c:forEach>

    <form action="<c:url value="/deal/"/>" method="POST">
        <input name="id" type="hidden" value=${deal.id}>
        <input name="status" type="hidden" value=${deal.status}>
        <td colspan="1">
            <label>
                <input name="name" type="text" value="${deal.name}">
            </label>
        </td>
        <td colspan="1">
            <label>
                <input name="description" type="text" value="${deal.description}">
            </label>
        </td>
        <td colspan="2">
            <input type="submit">
        </td>
    </form>

</table>

<a href="/deal/adddeals">
    <img src="../static/addtestdeals.png"/>
</a>

</body>
</html>
